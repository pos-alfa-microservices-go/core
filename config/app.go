package config

type AppConfig struct {
	Server   HTTPServer
	Database Database
	JWT      JWT
	RabbitMQ RabbitMQ
}

type HTTPServer struct {
	Port int
}

type Database struct {
	User     string
	Password string
	Host     string
	Port     int
	Name     string
}

type JWT struct {
	Secret string
}

type RabbitMQ struct {
	Url      string
	Port     int
	User     string
	Password string
	Queues   []Queue
}

type Queue struct {
	Name    string
	Durable bool
}

type API struct {
	URL string
}
