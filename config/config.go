package config

import "github.com/spf13/viper"

func Start() (*AppConfig, error) {
	if err := Read(); err != nil {
		return nil, err
	}
	appConfig := AppConfig{}
	viper.Unmarshal(&appConfig)

	return &appConfig, nil
}

func Read() error {
	viper.SetConfigName("env")
	viper.AddConfigPath("configs")
	viper.SetConfigType("yaml")
	viper.AutomaticEnv()
	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	return nil
}
