package server

import (
	"gitlab.com/pos-alfa-microservices-go/core/config"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func ValidateJWTMiddleware(appConfig *config.AppConfig) echo.MiddlewareFunc {
	return middleware.JWT([]byte(appConfig.JWT.Secret))
}
