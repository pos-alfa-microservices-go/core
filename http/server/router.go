package server

import (
	"encoding/json"
	"fmt"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Router interface {
	Create() *echo.Echo
}

func NewCoreEcho() *echo.Echo {
	e := echo.New()
	e.Use(middleware.Logger())

	errorHandler := NewCustomErrorHandler(e)
	e.HTTPErrorHandler = errorHandler.ErrorHandler

	return e
}

func PrintRoutes(e *echo.Echo) {
	data, err := json.MarshalIndent(e.Routes(), "", "  ")
	if err != nil {
		panic(err)
	}

	fmt.Println(string(data))
}
