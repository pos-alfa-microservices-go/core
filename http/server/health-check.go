package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

type message struct {
	Status string `json:"status"`
}

type HealhCheck interface {
	Check(c echo.Context) error
}

type DefautlHealhCheck struct {
}

func NewDefautlHealhCheck() HealhCheck {
	return &DefautlHealhCheck{}
}

func (h DefautlHealhCheck) Check(c echo.Context) error {
	return c.JSON(http.StatusOK, &message{
		Status: "OK",
	})
}
