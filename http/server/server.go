package server

import (
	"fmt"

	"gitlab.com/pos-alfa-microservices-go/core/log"
)

type Server interface {
	Start(router Router) error
}

type HttpServer struct {
	port int
}

func NewHttpServer(port int) Server {
	return &HttpServer{
		port: port,
	}
}

func (h HttpServer) Start(router Router) error {
	e := router.Create()
	PrintRoutes(e)

	address := fmt.Sprintf(":%d", h.port)
	log.Logger.Infof("starting httpserver on port: %s", address)
	return e.Start(address)
}
