package server

import (
	"errors"
	"net/http"

	commonsErrors "gitlab.com/pos-alfa-microservices-go/core/errors"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type CustomErrorHandler struct {
	e *echo.Echo
}

func NewCustomErrorHandler(e *echo.Echo) *CustomErrorHandler {
	return &CustomErrorHandler{e: e}
}

func (h *CustomErrorHandler) ErrorHandler(err error, c echo.Context) {
	if c.Response().Committed {
		return
	}

	if err == middleware.ErrJWTMissing {
		c.JSON(http.StatusUnauthorized, "")
		return
	}

	if errors.Is(err, commonsErrors.ErrInvalidUser) {
		c.JSON(http.StatusUnauthorized, "")
		return
	}

	if errors.Is(err, commonsErrors.ErrEmptyIdParam) {
		message := map[string]string{"message": commonsErrors.ErrEmptyIdParam.Error()}
		c.JSON(http.StatusBadRequest, message)
		return
	}

	if ve, ok := err.(*commonsErrors.ValidationError); ok {
		message := map[string]string{"message": ve.Message}
		c.JSON(http.StatusBadRequest, message)
		return
	}

	if ic, ok := err.(*commonsErrors.InvalidConstraint); ok {
		message := map[string]string{"message": ic.Message}
		c.JSON(http.StatusBadRequest, message)
		return
	}

	if de, ok := err.(*commonsErrors.DatabaseError); ok {
		message := map[string]string{"message": de.Message}
		c.JSON(http.StatusBadRequest, message)
		return
	}

	if br, ok := err.(*commonsErrors.BadRequestError); ok {
		message := map[string]string{"message": br.Message}
		c.JSON(http.StatusBadRequest, message)
		return
	}

	h.e.DefaultHTTPErrorHandler(err, c)
}
