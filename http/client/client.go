package client

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"

	"gitlab.com/pos-alfa-microservices-go/core/auth"
	commonsErrors "gitlab.com/pos-alfa-microservices-go/core/errors"
)

type HttpClient interface {
	Post(context.Context, string, interface{}) ([]byte, error)
	Get(context.Context, string) ([]byte, error)
}

type RestClient struct {
	httpClient    http.Client
	authorization bool
}

func NewRestClient(httpClient http.Client, authorization bool) HttpClient {
	return RestClient{
		httpClient:    httpClient,
		authorization: authorization,
	}
}

func (c RestClient) Post(ctx context.Context, url string, requestBody interface{}) ([]byte, error) {
	postBody, err := json.Marshal(requestBody)
	if err != nil {
		return nil, err
	}

	req, err := c.newRequest(ctx, http.MethodPost, url, bytes.NewBuffer(postBody))
	if err != nil {
		return nil, err
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode > 400 {
		return nil, errors.New("invalid request. status code: " + resp.Status)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == 400 {
		return nil, &commonsErrors.BadRequestError{
			Message: string(body),
		}
	}

	return body, nil
}

func (c RestClient) Get(ctx context.Context, url string) ([]byte, error) {
	req, err := c.newRequest(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode > 299 {
		return nil, errors.New("invalid request. status code: " + resp.Status)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

func (c RestClient) newRequest(ctx context.Context, method, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	if c.authorization {
		token, err := auth.GetTokenFromContext(ctx)
		if err != nil {
			return nil, err
		}

		req.Header.Set("Authorization", "Bearer "+token)
	}

	req.Header.Set("Content-Type", "application/json")

	return req, nil
}
