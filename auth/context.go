package auth

import (
	"context"
	"errors"
)

type key string

const (
	tokenCtxKey key = "token"
)

func NewContext(ctx context.Context, token string) context.Context {
	return context.WithValue(ctx, tokenCtxKey, token)
}

func GetTokenFromContext(ctx context.Context) (string, error) {
	token, ok := ctx.Value(tokenCtxKey).(string)
	if !ok {
		return "", errors.New("token not found inside context")
	}

	return token, nil
}
