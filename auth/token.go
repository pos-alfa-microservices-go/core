package auth

import (
	"context"
	"time"

	"gitlab.com/pos-alfa-microservices-go/core/config"

	"github.com/golang-jwt/jwt"
)

const (
	expirationTime = time.Hour * 1
)

type TokenManager interface {
	NewToken(login, role string) (*JWT, error)
	AddSystemTokenInContext(ctx context.Context) (context.Context, error)
}

type JWTTokenManager struct {
	appConfig *config.AppConfig
}

func NewJWTTokenManager(appConfig *config.AppConfig) TokenManager {
	return &JWTTokenManager{
		appConfig: appConfig,
	}
}

func (m JWTTokenManager) NewToken(login, role string) (*JWT, error) {
	expiration := time.Now().Add(expirationTime)
	info := jwt.MapClaims{}
	info["authorized"] = true
	info["login"] = login
	info["role"] = role
	info["exp"] = expiration.Unix()

	jwt := jwt.NewWithClaims(jwt.SigningMethodHS256, info)
	token, err := jwt.SignedString([]byte(m.appConfig.JWT.Secret))
	if err != nil {
		return nil, err
	}

	return &JWT{Token: token, Expiration: expiration}, nil
}

func (m JWTTokenManager) AddSystemTokenInContext(ctx context.Context) (context.Context, error) {
	jwt, err := m.NewToken("msinternal", "SYSTEM")
	if err != nil {
		return nil, err
	}

	return NewContext(ctx, jwt.Token), nil
}
