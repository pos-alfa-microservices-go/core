package rabbitmq

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/streadway/amqp"
)

type MessagePublisher interface {
	Publish(queueName string, message interface{}) error
}

type RabbitPublisher struct {
	client *RabbitClient
}

func NewRabbitPublisher(client *RabbitClient) MessagePublisher {
	return &RabbitPublisher{
		client: client,
	}
}

func (rp *RabbitPublisher) Publish(queueName string, message interface{}) error {
	if _, ok := rp.client.queues[queueName]; !ok {
		return errors.New("queue not declared " + queueName)
	}

	body, err := json.Marshal(message)
	if err != nil {
		return fmt.Errorf("fail to marshal %v to %s queue. %w", message, queueName, err)
	}

	return rp.client.ch.Publish(
		"",
		queueName,
		false,
		false,
		amqp.Publishing{
			Body:         body,
			DeliveryMode: amqp.Persistent,
		})
}
