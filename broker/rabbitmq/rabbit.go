package rabbitmq

import (
	"fmt"

	"gitlab.com/pos-alfa-microservices-go/core/config"
	"gitlab.com/pos-alfa-microservices-go/core/log"

	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

type RabbitClient struct {
	conn   *amqp.Connection
	ch     *amqp.Channel
	queues map[string]bool
}

func (rc *RabbitClient) Stop() {
	rc.conn.Close()
	rc.ch.Close()
}

func StartRabbitClient(appConfig *config.AppConfig) (*RabbitClient, error) {
	url := fmt.Sprintf("amqp://%s:%s@%s:%d/", appConfig.RabbitMQ.User, appConfig.RabbitMQ.Password, appConfig.RabbitMQ.Url, appConfig.RabbitMQ.Port)
	conn, err := amqp.Dial(url)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to connect to RabbitMQ")
	}

	ch, err := conn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "Failed to open a channel")
	}

	queues := make(map[string]bool, 0)
	for _, q := range appConfig.RabbitMQ.Queues {
		queues[q.Name] = q.Durable
	}

	rabbitClient := &RabbitClient{
		conn:   conn,
		ch:     ch,
		queues: queues,
	}

	return rabbitClient, nil
}

func startQueues(rabbitClient *RabbitClient) error {
	for queueName, durable := range rabbitClient.queues {
		dlxName := queueName + ".dlx"

		if err := declareExchange(rabbitClient.ch, dlxName, "fanout"); err != nil {
			return errors.Wrap(err, "Failed to declare dlq exchange: "+dlxName)
		}

		if err := declareQueue(rabbitClient.ch, queueName, durable, amqp.Table{"x-dead-letter-exchange": dlxName}); err != nil {
			return errors.Wrap(err, "Failed to declare queue: "+queueName)
		}

		if err := declareQueue(rabbitClient.ch, dlxName, durable, nil); err != nil {
			return errors.Wrap(err, "Failed to declare queue: "+dlxName)
		}

		if err := bindQueue(rabbitClient.ch, dlxName, queueName, dlxName); err != nil {
			return errors.Wrap(err, "Failed to declare queue: "+dlxName)
		}
	}

	log.Logger.Infof("rabbitMQ connected. queues: %v", rabbitClient.queues)
	return nil
}

func declareExchange(ch *amqp.Channel, name, kind string) error {
	return ch.ExchangeDeclare(
		name,
		kind,
		false,
		false,
		false,
		false,
		nil,
	)
}

func declareQueue(ch *amqp.Channel, name string, durable bool, args amqp.Table) error {
	_, err := ch.QueueDeclare(
		name,
		durable,
		false,
		false,
		false,
		args,
	)
	return err
}

func bindQueue(ch *amqp.Channel, name, key, exchange string) error {
	return ch.QueueBind(
		name,
		key,
		exchange,
		false,
		nil)
}
