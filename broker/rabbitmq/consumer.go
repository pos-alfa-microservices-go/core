package rabbitmq

import (
	"errors"
	"fmt"

	"gitlab.com/pos-alfa-microservices-go/core/log"
)

type MessageConsumer interface {
	Consume(queueName string, process func([]byte) error) error
}

type RabbitConsumer struct {
	client       *RabbitClient
	consumerName string
}

func NewRabbitConsumer(client *RabbitClient, consumerName string) MessageConsumer {
	return &RabbitConsumer{
		client:       client,
		consumerName: consumerName,
	}
}

func (rc *RabbitConsumer) Consume(queueName string, process func([]byte) error) error {
	if err := startQueues(rc.client); err != nil {
		return err
	}

	if _, ok := rc.client.queues[queueName]; !ok {
		return errors.New("queue not declared " + queueName)
	}

	msgs, err := rc.client.ch.Consume(
		queueName,
		rc.consumerName,
		false,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		return fmt.Errorf("fail to start consume %s. %w", queueName, err)
	}

	go func() {
		for msg := range msgs {
			body := msg.Body
			log.Logger.Infof("received a message: %s", string(body))

			err := process(body)
			if err != nil {
				log.Logger.Errorf("fail on receive message: %s", string(body), err)
				msg.Nack(false, false)
				continue
			}

			msg.Ack(false)
		}
	}()

	log.Logger.Infof("starting to consume: %s", queueName)

	return nil
}
