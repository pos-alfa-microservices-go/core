package database

import (
	"context"
	"errors"
	"fmt"

	coreErrors "gitlab.com/pos-alfa-microservices-go/core/errors"
	"gitlab.com/pos-alfa-microservices-go/core/log"

	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type key string

type QueryMapper struct {
	ScanParameters []interface{}
	MapResult      func() interface{}
}

type BatchQuery struct {
	Sql  string
	Args []interface{}
}

const (
	transactionKey          key = "TX"
	constraintViolationCode     = "23505"
)

type DatabaseManager interface {
	Exec(ctx context.Context, sql string, arguments ...interface{}) (commandTag pgconn.CommandTag, err error)
	QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row
	Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error)
	Batch(ctx context.Context, queries []BatchQuery) (pgx.BatchResults, error)
	RunInTransaction(ctx context.Context, do func(ctx context.Context) error) error
	HandlerError(err error) error
}

type DatabaseManagerImpl struct {
	dbPool *pgxpool.Pool
}

func NewDatabaseManagerImpl(pool *pgxpool.Pool) DatabaseManager {
	return &DatabaseManagerImpl{dbPool: pool}
}

func (d *DatabaseManagerImpl) RunInTransaction(ctx context.Context, do func(ctx context.Context) error) error {
	tx, err := d.dbPool.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return err
	}

	defer func() {
		if r := recover(); r != nil {
			log.Logger.Error(fmt.Sprintf("[ERROR] transaction fail... %v", r))
			tx.Rollback(ctx)
		}
	}()

	ctxWithTx := context.WithValue(ctx, transactionKey, tx)
	if err := do(ctxWithTx); err != nil {
		tx.Rollback(ctx)
		return err
	}

	return tx.Commit(ctx)
}

func (d *DatabaseManagerImpl) Exec(ctx context.Context, sql string, args ...interface{}) (commandTag pgconn.CommandTag, err error) {
	p, tx := d.connection(ctx)

	if tx != nil {
		commandTag, err = tx.Exec(ctx, sql, args...)
		if err != nil {
			return nil, d.HandlerError(err)
		}

		return
	}

	commandTag, err = p.Exec(ctx, sql, args...)
	if err != nil {
		return nil, d.HandlerError(err)
	}

	return
}

func (d *DatabaseManagerImpl) QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row {
	p, tx := d.connection(ctx)

	if tx != nil {
		return tx.QueryRow(ctx, sql, args...)
	}

	return p.QueryRow(ctx, sql, args...)
}

func (d *DatabaseManagerImpl) Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error) {
	p, tx := d.connection(ctx)

	if tx != nil {
		return tx.Query(ctx, sql, args...)
	}

	return p.Query(ctx, sql, args...)
}

func (d *DatabaseManagerImpl) connection(ctx context.Context) (*pgxpool.Pool, pgx.Tx) {
	tx, ok := ctx.Value(transactionKey).(pgx.Tx)
	if ok {
		return nil, tx
	}

	return d.dbPool, nil
}

func (d *DatabaseManagerImpl) Batch(ctx context.Context, queries []BatchQuery) (pgx.BatchResults, error) {
	batch := &pgx.Batch{}
	var batchResults pgx.BatchResults

	for _, query := range queries {
		batch.Queue(query.Sql, query.Args...)
	}

	_, tx := d.connection(ctx)
	if tx == nil {
		return nil, errors.New("Batch execution require transaction")
	}

	batchResults = tx.SendBatch(ctx, batch)

	return batchResults, nil
}

func (d DatabaseManagerImpl) HandlerError(err error) error {
	if pgError, ok := err.(*pgconn.PgError); ok {
		if pgError.Code == constraintViolationCode {
			return &coreErrors.InvalidConstraint{
				Message: pgError.Detail,
			}
		}

		return &coreErrors.DatabaseError{
			Code:    pgError.Code,
			Message: pgError.Message,
			Detail:  pgError.Detail,
		}
	}

	return err
}
