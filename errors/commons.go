package error

import "errors"

var (
	ErrEmptyIdParam = errors.New("empty id parameter")
	ErrInvalidUser  = errors.New("invalid user or password")
)

type ValidationError struct {
	Message string
}

func (ve ValidationError) Error() string {
	return ve.Message
}

type InvalidConstraint struct {
	Message string
}

func (ic InvalidConstraint) Error() string {
	return ic.Message
}

type DatabaseError struct {
	Code    string
	Message string
	Detail  string
}

func (e DatabaseError) Error() string {
	return e.Detail
}

type BadRequestError struct {
	Message string
}

func (br BadRequestError) Error() string {
	return br.Message
}
